<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Api
 *
 * @author In House Dev Program
 */
class Api {

    // URL for SOAPCLIENT
    protected $_url;

    // URL for CURL
    protected $_curl;

    protected $_proxy;
    protected $_proxy_host;
    protected $_proxy_port;


    // Authentication
    protected $_clientId;
    protected $_username;
    protected $_password;

    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->_url = $this->CI->config->item('api_url');
        $this->_curl = $this->CI->config->item('api_curl');
        $this->_clientId = $this->CI->config->item('api_client_id');
        $this->_username = $this->CI->config->item('api_username');
        $this->_password = $this->CI->config->item('api_password');
        $this->_proxy = $this->CI->config->item('api_proxy');
        $this->_proxy_host = $this->CI->config->item('api_proxy_host');
        $this->_proxy_port = $this->CI->config->item('api_proxy_port');
    }


    private function setSoapClientOptions() {
        $soapOptions = [];

        $stream_context = stream_context_create(
            array(
                'ssl' => array(
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                )
            )
        );

        if($this->_proxy == TRUE) {
            $soapOptions['trace'] = 1;
            $soapOptions['proxy_host'] = $this->_proxy_host;
            $soapOptions['proxy_port'] = $this->_proxy_port;
            $soapOptions['stream_context'] = $stream_context;
        } else {
            $soapOptions['trace'] = 1;
        }

        return $soapOptions;
    }



    public function downloadAntenna($antennaParam = NULL) {
        $params = array(
            'antennaParam' => $antennaParam,
            'clientId' => $this->_clientId,
            'username' => $this->_username,
            'password' => $this->_password
        );

        $client = new SoapClient($this->_url, $this->setSoapClientOptions());
        return $client->downloadAntenna($params);
    }

    public function downloadDocument($downloadDocumentParam = NULL) {
        $params = array(
            'downloadDocumentParam' => $downloadDocumentParam,
            'clientId' => $this->_clientId,
            'username' => $this->_username,
            'password' => $this->_password
        );

        $client = new SoapClient($this->_url, $this->setSoapClientOptions());
        return $client->downloadDocument($params);
    }

    public function downloadEquipment($equipmentParam = NULL) {
        $params = array(
            'equipmentParam' => $equipmentParam,
            'clientId' => $this->_clientId,
            'username' => $this->_username,
            'password' => $this->_password
        );

        $client = new SoapClient($this->_url, $this->setSoapClientOptions());
        return $client->downloadEquipment($params);
    }

    public function exportXML($licenseNumber, $apRefNumber) {
        $params = array(
            'licenseNumber' => $licenseNumber,
            'apRefNumber' => $apRefNumber,
            'clientId' => $this->_clientId,
            'username' => $this->_username,
            'password' => $this->_password
        );

        $client = new SoapClient($this->_url, $this->setSoapClientOptions());
        return $client->exportXML($params);
    }

    public function getApplication($applicationParam = NULL) {
        $params = array(
            'applicationParam' => $applicationParam,
            'clientId' => $this->_clientId,
            'username' => $this->_username,
            'password' => $this->_password
        );

        $client = new SoapClient($this->_url, $this->setSoapClientOptions());
        return $client->getApplication($params);
    }

    public function getDocumentInfo($documentParam = NULL) {
        $params = array(
            'documentParam' => $documentParam,
            'clientId' => $this->_clientId,
            'username' => $this->_username,
            'password' => $this->_password
        );

        $client = new SoapClient($this->_url, $this->setSoapClientOptions());
        return $client->getDocumentInfo($params);
    }

    public function getInvoice($invoiceParam = NULL) {
        $params = array(
            'invoiceParam' => $invoiceParam,
            'clientId' => $this->_clientId,
            'username' => $this->_username,
            'password' => $this->_password
        );

        $client = new SoapClient($this->_url, $this->setSoapClientOptions());
        return $client->getInvoice($params);
    }

    public function getMasterAddressData($masterAddressDataParam = NULL) {
        $params = array(
            'masterAddressDataParam' => $masterAddressDataParam,
            'clientId' => $this->_clientId,
            'username' => $this->_username,
            'password' => $this->_password
        );

        $client = new SoapClient($this->_url, $this->setSoapClientOptions());
        return $client->getMasterAddressData($params);
    }

    public function getWebserviceInfo() {
        $params = array(
            'clientId' => $this->_clientId,
            'username' => $this->_username,
            'password' => $this->_password,
        );

        $client = new SoapClient($this->_url, $this->setSoapClientOptions());
        return $client->getWebServiceInfo($params);
    }


    /**
     * Import XML
     *
     * @param type $xmlRequest
     * @return type
     */
    public function importXML($xmlRequest) {

        $ex = [];
        $content = NULL;

        $headers = array(
            "Content-type: text/xml"
        );

        try {
            $ch = curl_init();
            if($this->_proxy == TRUE) {
                curl_setopt($ch, CURLOPT_PROXY, $this->_proxy_host.':'.$this->_proxy_port);
            }
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL, $this->_curl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_TIMEOUT, 300);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlRequest); // the SOAP request
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $xmlResponse = curl_exec($ch);

            if(curl_error($ch)) {
                $ex[] = [
                    'code' => curl_errno($ch),
                    'message' => curl_error($ch)
                ];
            } else {
                $content = $this->xmlToObject($xmlResponse);
            }
        } catch (Exception $e) {
            $ex[] = [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
        curl_close($ch);

        return [
            'error' => $ex,
            'content' => $content
        ];
    }

    public function reviewStations($reviewStationParam = NULL) {
        $params = array(
            'reviewStationParam' => $reviewStationParam,
            'clientId' => $this->_clientId,
            'username' => $this->_username,
            'password' => $this->_password,
        );

        $client = new SoapClient($this->_url, $this->setSoapClientOptions());
        return $client->reviewStations($params);
    }


    private function xmlToObject($xmlResponse) {
        $contents = str_replace('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><ns2:importXMLResponse xmlns:ns2="http://webservice.SPweb.Products.lsgermany.de/">', '', $xmlResponse);
        $content = str_replace('</ns2:importXMLResponse></S:Body></S:Envelope>', '', $contents);
        $object = new SimpleXMLElement($content);

        return $object;
    }


}
