<!DOCTYPE html>
<html lang="en" class="no-js">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NEISA | REPORT REMEDY</title>
    <meta name="description" content="A sidebar menu as seen on the Google Nexus 7 website" />
    <meta name="keywords" content="google nexus 7 menu, css transitions, sidebar, side menu, slide out menu" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/popup.css');?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/google/css/normalize.css');?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/google/css/demos.css');?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/google/css/component.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/plugins/bootstrap/css/bootstrap.min.css')?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('asset/dist/css/adminlte.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/css/style.css')?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url('asset/plugins/iCheck/flat/blue.css')?>">
     <!-- *** START TAMBAH IMPORT FONTAWESOME IYON *** -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/fontawesome-free-5.6.3-web/css/solid.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/plugins/fontawesome-free-5.6.3-web/css/brands.css');?>">
    <!-- *** END TAMBAH IMPORT FONTAWESOME IYON *** -->

    <style media="screen">
      .nav-button{
        color: #007bff;
        margin-left: -3px;
      }
      .nav-button:hover {
        color: #0056b3 !important;
      }
        /* ** START TAMBAH CSS IYON** */
      .dropClass {
        /* display: none; */
        visibility: hidden;
        opacity: 0;
        transition: 
        all .5s ease;
        background-color: transparent;
        min-width: 160px;
        overflow: auto;
        z-index: 1;
        height: 0;
        /* margin-bottom: -5%; */
      }
      .dropClass a {
        color: black;
        padding: 12px 16px;
        /* text-decoration: none; */
        display: block;
      }
      .dropClass a:hover {background-color: rgba(255,255,255,.1);}
      .show {
        /* display: block; */
        visibility: visible;
        opacity: 1;
        height: auto;
        padding: 0.5rem 1rem;
        background-color: rgba(255,255,255,.3);
        margin-bottom: 1.5%;
        border-radius: 3%;
      }
      .putar {
        transform: rotate(90deg);
        transition: all .5s ease;
      }
      .brand-image {
        line-height: .8;
        max-height: 53px;
        width: auto;
        margin-left: 0.7rem;
        margin-right: .5rem;
        margin-top: -3px;
        float: none;
        opacity: .9;
      }
      .backgroundImg {
        width: auto;
        height: 100%;
        opacity: 1;
        position: absolute;
      }
      .backgroundImg2 {
        position: fixed;
        width: 100%;
        max-height: 56px;
        margin-left: -2%;
        opacity: 1;
      }
      .nav-item:hover {
        background-color: rgba(255,255,255,.3);
        border-radius: 5%;
        transition: all .2s ease;
      }
      .active {
        background-color: rgba(243, 255, 226, .8) !important;
        color: #343a40 !important;
        font-weight: 600;
      }
      .berisik {
        min-height:500px !important
      }
      #myChart, #myChart2 {
        max-height:200px !important;
        width: auto !important;
        /* height: 180px !important; */
      }
      /* START BIKIN UI BARU */
      #bungkus {
        background: url(http://10.54.36.49/apk-report/asset/dist/img/darkwall6.jpg) center center;
      }
      .card{
        background-color: transparent !important;
        color: #fff !important;
      }
      .card.chartcard {
        background-color:transparent;
        border: 0;
        border-radius: 0;
        box-shadow: none;
      }
      .boksHead {
        font-size: 17px;
        /* box-shadow: 0 3px 1px 0 rgba(0, 0, 0, 0.2), 0 1px 0px 0 rgba(0, 0, 0, 0.19); */
        padding: 10px;
        font-weight: 500;
        border-radius: 0;
        /* background-color: rgba(255, 255, 255, .2); */
        /* background-color: rgba(150, 178, 138, .5); */
        color: #96b28a;
        font-weight: bold;
        box-shadow: none;
      }
      .boksBody {
        border-radius: 0;
        background-color: rgba(255, 255, 255, .1);
        /* background-image: linear-gradient(to top, rgba(0,255,255,.2), rgba(150, 178, 138, .5)); */
        /* background-color: rgba(0, 0, 0, .5); */
        box-shadow: none;
      }
      #dropdownMenuButton {
        background-color: rgba(255, 255, 255, .1);
        color:#fff;
        border: 0
      }
      .table-responsive.mailbox-messages {
        width: 100%; 
        font-size: 12px; 
        margin:auto;
        background-image: linear-gradient(to top, rgba(49, 113, 160,.5), rgba(1, 14, 23,.8));
      }
      /* tr:hover {
        background-color: rgba(255, 255, 255, .2)
      } */
      tr > td:hover {
        background-color: rgba(255, 255, 255, .9)
      }
        /* BREADCRUMB */
          .breadcrumb {
              background-color: transparent;
          }
          .breadcrumb-item.active {
              background-color: transparent !important;
              font-size: larger;
              color: #fff !important;
          }
          li.breadcrumb-item > a,  li.breadcrumb-item > a:hover{
              color: #fff
          }
          li.breadcrumb-item > a:hover {
              /* background-color: rgba(255,255,255,.3); */
              border-bottom:2px solid;
              border-radius: 5%;
              transition: all .1s ease;
          }
          ol {
              list-style-position:inside;
              margin:0;
              padding:0;
          }
          li.breadcrumb-item.active {
              border-bottom:2px solid;
              border-bottom-color: rgba(150, 178, 138, 1);
          }
        /* BREADCRUMB */
        
      /* END BIKIN UI BARU */
      /* ** END TAMBAH IYON** */
    </style>

    <script src="<?php echo base_url('asset/plugins/google/js/modernizr.custom.js');?>">
    </script>
  </head>


 <body class="hold-transition sidebar-mini" style="background: #f4f6f9; color: white;">
    <nav class="main-header navbar navbar-expand bg-dark" style="margin-left: 250px; position:fixed; width:100%;">
      <img src="<?php echo base_url('asset/dist/img/wall5.jpg');?>" class="backgroundImg2" style="position: fixed;
      width: 100%;">
      <!-- <img src="./asset/dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;
      width: 100%;"> -->
      <!-- Left navbar links -->
      <ul class="navbar-nav" style="z-index: 999;">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars nav-button" style="color:white;"></i></a>
        </li>
        <li class="navbar-brand" style="color:white;margin-left: 10%;">NEISA | REPORT REMEDY</li>
        <li class="nav-item">
          <a class="nav-link btn btn-lg" href="http://10.54.36.49/landingPage/" onclick="sessionStorage.clear();" style="
                  color: #343a40 !important;
                  background-color: #fff;
                  position: fixed;
                  font-size: 10px;
                  right: 1%;
                  height: auto;
                  box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                  text-transform: uppercase;
                  font-family: Roboto;
                  padding: 1%;"><i class="fa fa-sign-out-alt"></i> Log Out</a>
        </li>
      </ul>
    </nav>
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4 berisik" >
      <img src="<?php echo base_url('asset/dist/img/wall3.jpg');?>" class="backgroundImg">
      <!-- <img src="./asset/dist/img/wall3.jpg" class="backgroundImg">    -->
      <!-- Brand Logo -->
      <a href="#" class="brand-link">
        <img src="<?php echo base_url('asset/dist/img/telkomsel.png');?>" alt="AdminLTE Logo" class="brand-image"
             style="opacity: .8; float:none; widht:200px; line-height:.8; max-height:53px;margin-left:0.7rem;margin-right:.5rem;margin-top:-3px">
      </a>
      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                <a href="http://10.54.36.49/dashboard-bts-on-air/public/" class="nav-link" style="color: #fff;padding: 0.5rem 1rem !important;">
                  <i class="nav-icon fa fa-home"></i>
                    <p style="margin-left: 3px;">Dashboard</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="http://10.54.36.49/dashboard-license" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                  <i class="nav-icon fa fa-newspaper"></i>
                    <p style="margin-left: 3px;">License</p>
                </a>
            </li>
            <!-- <li class="nav-item">
                <a href="http://10.54.36.49/btsonair" class="nav-link " style="color: #fff;padding: 0.5rem 1rem !important;">
                <i class="nav-icon fa fa-broadcast-tower"></i>
                    <p style="margin-left: 3px;">BTS Status</p>
                </a>
            </li> -->
            <li class="nav-item" style="cursor: pointer;">
                <a onclick="dropDead()" class="nav-link aa dropbtn" style="color: #fff;padding: 0.5rem 1rem !important;">
                    <i class="nav-icon fa fa-angle-right" id="dropIcon"></i>
                    <p style="margin-left: 3px;">Create</p>
                </a>
            </li>
            <div class="dropClass" id="dropId">
              <li class="nav-item">
                <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                    <i class="nav-icon fa fa-list-alt"></i>
                    <p style="margin-left: 3px;">Create Integrasi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                    <i class="nav-icon fa fa-list-alt"></i>
                    <p style="margin-left: 3px;">Create Rehoming</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="http://10.54.36.49/apk-nodin-dismantle/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                  <i class="nav-icon fa fa-list-alt"></i>
                    <p style="margin-left: 3px;">Create Dismantle</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="http://10.54.36.49/apk-nodin-relocation/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                  <i class="nav-icon fa fa-list-alt"></i>
                    <p style="margin-left: 3px;">Create Relocation</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="http://10.54.36.49/apk-nodin-swap/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                  <i class="nav-icon fa fa-list-alt"></i>
                  <p style="margin-left: 3px;">Create Swap</p>
                  </a>
              </li>
            </div>
            <li class="nav-item">
                <a href="http://10.54.36.49/change-front-2/public/" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                  <i class="nav-icon fa fa-project-diagram"></i>
                  <p style="margin-left: 3px;">Process Tracking</p>
                </a>
            </li>
            <li class="nav-item">
              <a href="http://10.54.36.49/tableList" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                <i class="nav-icon fa fa-table"></i>
                <p style="margin-left: 3px;">Nodin & MoM Report</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="http://10.54.36.49/apk-report/index.php/ReportController" class="nav-link aa active" style="color: #fff;padding: 0.5rem 1rem !important;">
                  <i class="nav-icon fa fa-book"></i>
                  <p style="margin-left: 3px;">Report Remedy</p>
              </a>
            </li>
          </ul>
        </nav>
      </div><!-- SIDEBAR -->
      <!-- /.sidebar -->
    </aside>
      <!-- /.col -->
      <!-- /.row -->
    <div class="content-wrapper" id="bungkus" style=" margin-left: 250px; margin-top: 50px;">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item active" aria-current="page">List Report Huawei</li>
        </ol>
      </nav>
      <div class="row" style="width:100%; margin:auto;">
        <div class="col-md-12">
          <div class="card bg-white">
          <div class="card-body " style="display:flex">

            <section class="col-lg-6">
              <!-- <div class="card" style="width: 85%;"> -->
              <div class="card chartcard">
                <div class="card-header boksHead">
                  <h3 style="color: white;" class="card-title">
                    <!-- <i class="fa fa-th mr-1"></i> -->
                    MOST REMEDY NOK (REGIONAL)
                  </h3>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse">
                      <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-widget="remove">
                      <i class="fa fa-times"></i>
                    </button>
                  </div>
                </div><!-- CARD HEADER -->
                <div class="card-body boksBody" >
                    <!-- @include('top_5_chart_monly') -->
                    <canvas id="myChart"></canvas>   
                </div><!-- CARD BODY -->
              </div><!-- CARD -->
            </section>
            <section class="col-lg-6">
              <!-- <div class="card" style="width: 85%;"> -->
              <div class="card chartcard">
                <div class="card-header boksHead">
                  <h3 style="color: white;" class="card-title">
                    <!-- <i class="fa fa-th mr-1"></i> -->
                    TOP 10 USER REMEDY (MONTHLY)
                  </h3>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-widget="collapse">
                      <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-widget="remove">
                      <i class="fa fa-times"></i>
                    </button>
                  </div>
                </div><!-- CARD HEADER -->
                <div class="card-body boksBody">
                    <canvas id="myChart2"></canvas>   
                </div><!-- CARD BODY -->
              </div><!-- CARD -->
            </section>  
          </div><!-- CARD BODY -->

          <div class="card-header">
            <div class="navbar-brand">List Remedy NOK
            </div>
            <div class="row">
              <div class="col-md-12">
                <!-- /.btn-year -->
                <div class="dropdown pull-right">
                  <label>Year: </label>
                  <button class="btn btn-default btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php
                        if(isset($_GET['year'])) {
                            echo '<a href='.base_url("index.php/ReportController/index").'>'.$_GET['year'].'</a>';
                          } 
                    ?>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php
                      foreach ($tahun as $datas){
                        if($datas->year != date('Y')) {
                          echo '<a class="dropdown-item" href='.base_url("index.php/ReportController/index?year=".$datas->year).'>'.$datas->year.'</a>';
                        } else {
                          echo '<a class="dropdown-item" href='.base_url("index.php/ReportController/index?year=".date('Y')).'>'.date('Y').'</a>';
                        }
                      }
                    ?>
                  </div>
                </div><!-- DROPDOWN PULL-RIGHT -->
              </div><!-- COL-MD-12 -->
            </div><!-- ROWY -->
          </div><!-- CARD HEADER -->

          <div class="table-responsive mailbox-messages">
            <table id="example2" class="table table-bordered table-hover">
              <thead  style="color:white;">
                <tr>
                  <th>Regional</th>
                  <th>January</th>
                  <th>February</th>
                  <th>March</th>
                  <th>April</th>
                  <th>May</th>
                  <th>June</th>
                  <th>July</th>
                  <th>August</th>
                  <th>September</th>
                  <th>October</th>
                  <th>November</th>
                  <th>December</th>
                </tr>
              </thead>
              <tbody>
                <?php   
                foreach ($huawei as $data) {
                  if ($data->regional != null){
                    echo "
                      <tr>

                        <td>$data->regional</td>

                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=01').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->January
                          </button></a></td>
                        
                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=02').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->February
                          </button></a></td>
                        
                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=03').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->March
                          </button></a></td>

                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=04').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->April
                          </button></a></td>

                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=05').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->May
                          </button></a></td>

                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=06').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->June
                          </button></a></td>

                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=07').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->July
                          </button></a></td>

                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=08').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->August
                          </button></a></td>

                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=09').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->September
                          </button></a></td>

                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=10').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->October
                          </button></a></td>

                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=11').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->November
                          </button></a></td>

                        <td><a href=".base_url('index.php/ReportController/index_detail?regional='.str_replace(' ', '_', $data->regional).'&month=12').">
                          <button class='btn btn-lg' style='color: black; text-transform: uppercase; position: center; background-color: rgba(255, 255, 255,.7); font-size: 12px; box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12); font-family: Roboto;'>
                            $data->December
                          </button></a></td>

                      </tr>
                    ";
                  } 
                }    
                ?>
              </tbody>
            </table>
            <!-- /.table -->
          </div><!-- TABLE-RESPONSIVE -->
        </div><!-- COL-MD-12 -->
      </div><!-- ROW -->
    </div><!-- CONTENT-WRAPPER -->
  </body>
  <footer class="main-footer sticky-bottom" style="margin-left: 0px; background: white ; color: black;">
    <strong style="font-size: 12px">Copyright &copy; 2018 <a style="color:white;">Telkomsel</a>.</strong>
    <div class="float-right d-none d-sm-inline-block">
    </div>
  </footer>

<script src="<?php echo base_url('asset/plugins/google/js/classie.js');?>"></script>
<script src="<?php echo base_url('asset/plugins/google/js/gnmenu.js');?>"></script>
<!-- jQuery -->
<script type="text/javascript" src="<?php echo base_url('/asset/plugins/jquery/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/plugins/jQueryUI/jquery-ui.min.js'); ?>"></script>
<!-- Bootstrap 4 -->
<script type="text/javascript" src="<?php echo base_url('asset/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- Slimscroll -->
<script type="text/javascript" src="<?php echo base_url('asset/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script type="text/javascript" src="<?php echo base_url('asset/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="<?php echo base_url('asset/dist/js/adminlte.min.js'); ?>"></script>
<!-- iCheck -->
<script type="text/javascript" src="<?php echo base_url('asset/plugins/iCheck/icheck.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('asset/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
<script>
  new gnMenu( document.getElementById( 'gn-menu' ) );
</script>
<script type="text/javascript" src="<?php echo base_url('asset/dist/js/plugins/chartjs2/Chart.js'); ?>"></script>
<script>                                                  
  function dropDead() {
    document.getElementById("dropIcon").classList.toggle('putar');
    document.getElementById("dropId").classList.toggle("show");
  };

  $(document).ready(function() {

    /**
    * call the data.php file to fetch the result from db table.
    */
    $.ajax({
      // url : "http://localhost:88/api-report/index.php/MostNokController",
      url : "http://10.54.36.49/api-report/MostNokController/",
      type : "GET",
      success : function(data){
        // console.log("api",data);

        var dataApi = {
            "bulan" : [],
            "reg"   : [],
          };
        var len = data.length;
        for (var i = 0; i < len; i++) {
          dataApi.bulan.push(data[i].count_regional)
          dataApi.reg.push(data[i].regional)
          }
      
      var ctx = document.getElementById("myChart");
      var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: dataApi.reg,
            // ["R1", "R2", "R3", "R4", "R5", "R6", "R7", "R8", "R9", "R10", "R11"],
            datasets: [{
                // label: '# of Votes',
                data: dataApi.bulan,
                backgroundColor: [
                    // 'rgba(27, 104, 134, 1)',
                    // 'rgba(103, 86, 138, 1)',
                    // 'rgba(196, 96, 124, 1)',
                    // 'rgba(175, 99, 127, 1)',
                    // 'rgba(250, 100, 127, 1)',
                    // 'rgba(239, 154, 115, 1)',
                    // 'rgba(236, 196, 101, 1)',
                    // 'rgba(127, 217, 191, 1)',
                    // 'rgba(156, 187, 169, 1)',
                    // 'rgba(201, 174, 139, 1)',
                    // 'rgba(28, 173, 254, 1)',
                    
                    'rgba(27, 104, 134, .5)',
                    'rgba(103, 86, 138, .5)',
                    'rgba(196, 96, 124, .5)',
                    'rgba(175, 99, 127, .5)',
                    'rgba(250, 100, 127, .5)',
                    'rgba(239, 154, 115, .5)',
                    'rgba(236, 196, 101, .5)',
                    'rgba(127, 217, 191, .5)',
                    'rgba(156, 187, 169, .5)',
                    'rgba(201, 174, 139, .5)',
                    'rgba(28, 173, 254, .5)',
                ],
                borderColor: [
                    'rgba(255, 255, 255, 0.5)',
                    'rgba(255, 255, 255, 0.5)',
                    'rgba(255, 255, 255, 0.5)',
                    'rgba(255, 255, 255, 0.5)',
                    'rgba(255, 255, 255, 0.5)',
                    'rgba(255, 255, 255, 0.5)',
                    'rgba(255, 255, 255, 0.5)',
                    'rgba(255, 255, 255, 0.5)',
                    'rgba(255, 255, 255, 0.5)',
                    'rgba(255, 255, 255, 0.5)',
                    'rgba(255, 255, 255, 0.5)',
                ],
                borderWidth: 1
            }]
        },
        
        options: 
        {
          
          legend: {
                // display: false;
                position: 'right',
                labels: {
                  // fontSize: 8,
                  fontColor: "#fff",
                }
            },
        }
      });
      }
    })

    $.ajax({
      //   url : "http://localhost:88/api-report/index.php/MostUserController",
      url : "http://10.54.36.49/api-report/MostUserController",
      type : "GET",
      success : function(data){
        console.log("api2",data);
        var dataApi = {
            "regional" : [],
            "user" : [],
          };
        // data.sort(function(a,b) {return (a.usernameReg > b.usernameReg) ? 1 : ((b.usernameReg > a.usernameReg) ? -1 : 0);} );
        data.sort(function(a,b) {return ( b.usernameReg - a.usernameReg)} );
        // console.log("api2 sort",data);

        var len = data.length;
        // console.log("len", len);

        for (var i = 0; i < len; i++) {
          dataApi.regional.push(data[i].usernameReg)
          dataApi.user.push(data[i].username)
        }
        // console.log("dataApi.regional", dataApi.regional)

        //  *** TESTING
        var topUserName = dataApi.user.slice(0,10);
        console.log( "topUserName", topUserName); 
        var topUser = dataApi.regional.slice(0,10);
        console.log( "topValues", topUser); 
        // *** END TESTING
        
        var ctx2 = document.getElementById("myChart2");
          var myChart2 = new Chart(ctx2, {
            type: 'pie',
            data: {
                labels: 
                topUserName,
                // dataApi.user,
                datasets: [{
                    // label: '# of Votes',
                    data: 
                    topUser,
                    // dataApi.regional,
                    backgroundColor: [
                        'rgba(27, 104, 134, .5)',
                        'rgba(103, 86, 138, .5)',
                        'rgba(196, 96, 124, .5)',
                        'rgba(175, 99, 127, .5)',
                        'rgba(250, 100, 127, .5)',
                        'rgba(239, 154, 115, .5)',
                        'rgba(236, 196, 101, .5)',
                        'rgba(127, 217, 191, .5)',
                        'rgba(156, 187, 169, .5)',
                        'rgba(201, 174, 139, .5)',
                    ],
                    borderColor: [
                        'rgba(255, 255, 255, 0.5)',
                        'rgba(255, 255, 255, 0.5)',
                        'rgba(255, 255, 255, 0.5)',
                        'rgba(255, 255, 255, 0.5)',
                        'rgba(255, 255, 255, 0.5)',
                        'rgba(255, 255, 255, 0.5)',
                        'rgba(255, 255, 255, 0.5)',
                        'rgba(255, 255, 255, 0.5)',
                        'rgba(255, 255, 255, 0.5)',
                        'rgba(255, 255, 255, 0.5)',
                    ],
                    borderWidth: 1
                }]
            },
            
            options: 
            {
              
              legend: {
                    // display: false;
                    position: 'right',
                    labels: {
                      fontSize: 12,
                      fontColor: "#fff",
                    }
                },
            //     scales: {
            //         yAxes: [{
            //             ticks: {
            //                 beginAtZero:true
            //             }
            //         }]
            //     }
            }
        });
      }
    })

  });

  (function ($) {
    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass   : 'iradio_flat-blue'
    })

    //Enable check and uncheck all functionality
    $('.checkbox-toggle').click(function () {
      var clicks = $(this).data('clicks')
      if (clicks) {
        //Uncheck all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').iCheck('uncheck')
        $('.fa', this).removeClass('fa-check-square-o').addClass('fa-square-o')
      } else {
        //Check all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').iCheck('check')
        $('.fa', this).removeClass('fa-square-o').addClass('fa-check-square-o')
      }
      $(this).data('clicks', !clicks)
    })

    //Handle starring for glyphicon and font awesome
    $('.mailbox-star').click(function (e) {
      e.preventDefault()
      //detect type
      var $this = $(this).find('a > i')
      var glyph = $this.hasClass('glyphicon')
      var fa    = $this.hasClass('fa')

      //Switch states
      if (glyph) {
        $this.toggleClass('glyphicon-star')
        $this.toggleClass('glyphicon-star-empty')
      }

      if (fa) {
        $this.toggleClass('fa-star')
        $this.toggleClass('fa-star-o')
      }
    })
  })(jQuery)
</script>
</html>