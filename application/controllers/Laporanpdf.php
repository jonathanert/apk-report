<?php
Class Laporanpdf extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->library('pdf');
    }
    
    public function index(){
        $pdf = new FPDF('l','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string
        $pdf->Cell(380,7,'LAPORAN NOTA DINAS',0,1,'C');

        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',7);
        $pdf->Cell(11,6,'Site ID',1,0);
        $pdf->Cell(20,6,'NE ID',1,0);
        $pdf->Cell(15,6,'Sector ID',1,0);
        $pdf->Cell(35,6,'Site Name',1,0);
        $pdf->Cell(14,6,'OSS Name',1,0);
        $pdf->Cell(23,6,'Kabupaten',1,0);
        $pdf->Cell(13,6,'LAC',1,0);
        $pdf->Cell(14,6,'Cell Name',1,0);
        $pdf->Cell(12,6,'CI/SAC',1,0);
        $pdf->Cell(22,6,'Scrambling Code',1,0);
        $pdf->Cell(10,6,'RNC',1,0);
        $pdf->Cell(10,6,'RNC ID',1,0);
        $pdf->Cell(15,6,'RNC SPC',1,0);
        $pdf->Cell(13,6,'RAC',1,0);
        $pdf->Cell(13,6,'URA ID',1,0);

        $pdf->Cell(16,6,'MSCS Name',1,0);
        $pdf->Cell(15,6,'MSCS SPC',1,0);
        $pdf->Cell(16,6,'MGW Name',1,0);
        $pdf->Cell(14,6,'MGW SPC',1,0);
        $pdf->Cell(13,6,'Locno',1,0);
        $pdf->Cell(15,6,'POC PSTN',1,0);
        $pdf->Cell(14,6,'Time Zone',1,0);
        $pdf->Cell(30,6,'SOW',1,0);
        $pdf->Cell(13,6,'Long',1,0);
        $pdf->Cell(13,6,'Lat',1,1);

        $pdf->SetFont('Arial','',7);
        $mahasiswa = $this->db->get('nodin')->result();
        foreach ($mahasiswa as $row){
            $pdf->Cell(11,6,$row->siteid,1,0);
            $pdf->Cell(20,6,$row->ne_id,1,0);
            $pdf->Cell(15,6,$row->sector_id,1,0);
            $pdf->Cell(35,6,$row->site_name,1,0);
            $pdf->Cell(14,6,$row->oss_name,1,0);
            $pdf->Cell(23,6,$row->kabupaten,1,0);
            $pdf->Cell(13,6,$row->lac,1,0);
            $pdf->Cell(14,6,$row->cell_name,1,0);
            $pdf->Cell(12,6,$row->sac,1,0);
            $pdf->Cell(22,6,$row->scrambling_code,1,0);
            $pdf->Cell(10,6,$row->rnc,1,0);
            $pdf->Cell(10,6,$row->rnc_id,1,0);
            $pdf->Cell(15,6,$row->rnc_scp,1,0);
            $pdf->Cell(13,6,$row->rac,1,0);
            $pdf->Cell(13,6,$row->ura_id,1,0);
            $pdf->Cell(16,6,$row->mscs_name,1,0);
            $pdf->Cell(15,6,$row->mscs_spc,1,0);
            $pdf->Cell(16,6,$row->mgw_name,1,0);
            $pdf->Cell(14,6,$row->mgw_spc,1,0);
            $pdf->Cell(13,6,$row->locno,1,0);
            $pdf->Cell(15,6,$row->poc_pstn,1,0);
            $pdf->Cell(14,6,$row->time_zone,1,0);
            $pdf->Cell(30,6,$row->sow,1,0);
            $pdf->Cell(13,6,$row->longitude,1,0);
            $pdf->Cell(13,6,$row->latitude,1,1);
        }
        $pdf->Output();
    }
}
?>