<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportController extends CI_Controller {

	var $API_Report ="";
	var $API_ReportTahunanHuawei = "";
	var $API_DetailRegionalHuawei = "";

	function __construct() {
		parent::__construct();
		$this->API_Report="http://10.54.36.49/api-report/ReportController/";
		// $this->API_Report="http://localhost:88/api-report/ReportController/";
		// $this->API_Report="http://localhost/application/api/ReportControllerTest/";
		$this->API_ReportTahunanHuawei="http://10.54.36.49/api-report/ReportPertahunHuawei/";
		$this->API_DetailRegionalHuawei="http://10.54.36.49/api-report/HuaweiController/";

		session_start();
		// $this->load->library('session');
		$this->load->library('pagination');
		$this->load->library('curl');
		$this->load->library('excel');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library("Nusoap_library", "");
	}

	public function index(){
		if (isset($_GET['year'])) {
	       $data['huawei'] = json_decode($this->curl->simple_get($this->API_Report.'?year='.$_GET['year'].''));
	       $data['tahun'] = json_decode($this->curl->simple_get($this->API_ReportTahunanHuawei.'?year='.$_GET['year'].''));
//	       $data['regional'] = json_decode($this->curl->simple_get($this->API_DetailRegionalHuawei.'?region='.$_GET['regional'].''));
	    } else {
	       $data['huawei'] = json_decode($this->curl->simple_get($this->API_Report.'?year='.date('Y').''));
	       $data['tahun'] = json_decode($this->curl->simple_get($this->API_ReportTahunanHuawei.'?year='.date('Y').''));
	    }

		$this->load->view('ReportRemedy', $data);
	}

	public function index_detail(){

		$regional = $_GET['regional'];

		if (isset($_GET['month'])) {
			$data['detail'] = json_decode($this->curl->simple_get($this->API_DetailRegionalHuawei.'?regional='.$_GET['regional'].'&month='.$_GET['month'].''));
		} else {
			$data['detail'] = json_decode($this->curl->simple_get($this->API_DetailRegionalHuawei.'?regional=empty&month=empty'));
		}
	
		$this->load->view('DetailNOK', $data); 
	}
}
